#!/bin/bash
QIIME_SCRIPTS_DIR=/usr/lib/qiime/bin
set -x
# verifies if qiime is already installed and available in the path
qiime_installed=`which print_qiime_config.py`
qiime_globally=`locate $QIIME_SCRIPTS_DIR/print_qiime_config.py`

if [  ${#qiime_installed} -eq 0 ] && [ ${#qiime_globally} -eq 0 ]; then
	echo "QIIME is not installed"
else
	echo "QIIME is installed"
	PACKAGES_DIR=`python scripts/python_packages_path.py`
	QIIME_GG_DIR=$PACKAGES_DIR/qiime_default_reference/gg_13_8_otus
	echo "QIIME default reference is located at $QIIME_GG_DIR"
	mkdir -p 16s_refs/qiime/gg
	ln -s $QIIME_GG_DIR/rep_set/97_otus.fasta 				16s_refs/qiime/gg/gg_otus.fasta
	ln -s $QIIME_GG_DIR/taxonomy/97_otu_taxonomy.txt 		16s_refs/qiime/gg/gg_otu_taxonomy.txt
fi

# create subdirs to receive the reference for each pipeline
mkdir -p 16s_refs/qiime
mkdir -p 16s_refs/mothur
mkdir -p 16s_refs/usearch




# verifies if we are running inside the gulbenkian network...
# it will download all the references from a AFS location and install them...


if [[  `hostname -A` =~ .*gulbenkian\.pt.*  ]] ; then
	echo "We are inside the IGC network!"
	. ./retrieve_16s_refs_from_afs.sh
else
	echo "We are not on the IGC nerwork!"
fi

set +x
# create and configure specific QIIME configuration parameters

. ./init-env.sh

set -x

if [ ! -z $SUB16S_ROOT_DIR  ] ; then

#	rm -rv $SUB16S_ROOT_DIR/qiime-confs

	mkdir -p $SUB16S_ROOT_DIR/qiime-confs

	for ref in $QIIME_DB_DIR/* ; do
		REF=`basename $ref`
		sed -e "s/\$REF/$REF/g" \
		 -e "s;\$QIIME_DB_DIR;$QIIME_DB_DIR;g" \
		 templates/qiime-config-use-xx.conf.tmpl \
		 > qiime-confs/qiime-config-use-$REF.conf
	done

fi

set +x
