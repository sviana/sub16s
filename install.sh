
#!/bin/bash

#view README.md

# create virtual environment ....

virtualenv sub16s-venv

# switch to it

. ./sub16s-venv/bin/activate

# install needed packages inside it
pip install pexpect orderedset simplejson biopython

# ask to install QIIME on the new virtualenv
. scripts/prompt_dwnld_qiime.sh

# configure for this host .... and create one file in hostconfigs/ dir

if [ ! -e hostconfigs/`hostname`.config ] ; then
	mkdir hostconfigs
	./create-new-hostconfig.sh
fi


# ensures the dependencies (mothur-wrapper and UnifracSim) are installed and properly set up

#clone unifracsim

git clone https://bitbucket.org/sviana/unifracsim.git

#builds unifracsim from class files
if [ ! -e  unifracsim/unifracsim.jar ] ; then
	ant -f unifracsim/build.xml build
fi

# clone mothur-wrapper

git clone https://github.com/digfish/mothur-wrapper.git

#downloads ncbitaxon.owl into unifracsim

if [ ! -e unifracsim/ncbitaxon.obo ] ; then
	wget http://purl.obolibrary.org/obo/ncbitaxon.obo -P unifracsim
fi

# calls script to setup the references

. ./setup_16s_refs.sh

# view docs/after_install.md
