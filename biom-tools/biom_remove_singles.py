#!//usr/bin/env python
"""
given a biom file, remove the OTUs with just one single occurrence ("singletons")
"""


import os, sys
import simplejson
import commons
import time
from orderedset import OrderedSet



def main():

    biom_filename = sys.argv[1]


    if biom_filename.find('.no_singletons') >= 0:
        print  biom_filename, " already processed!"
        return

    biom_data = simplejson.load( open(biom_filename,"r"  ),object_pairs_hook = simplejson.OrderedDict )

    print "Processing BIOM", biom_filename

    before = time.time()

    data = biom_data['data']

 #   print (biom_data.keys())

    singleton_coords = filter(lambda item: item[2] == 1,data)

    singleton_row_ids = [coord[0] for coord in  singleton_coords  ]

    rows = biom_data['rows']

    non_singleton_coords = filter(lambda item: item[2] !=1, data )


    print "Total rows before:", len(rows)

    for row_id in singleton_row_ids:
        rows[row_id] = None

    filtered_rows = filter ( lambda row: row , rows )

    print "Total singleton_coords:", len(singleton_coords)

    print "Total rows after removing singleton_coords" , len(filtered_rows)

    biom_data['rows'] = filtered_rows

    biom_data['shape'] = [len(filtered_rows) ,1]

    for i in xrange(0,len(non_singleton_coords) ):
        non_singleton_coords[i][0] = i

    biom_data['data'] = non_singleton_coords

    removed_singletons_count = len(data) - len(filtered_rows)

    print "Removed %d singleton_coords " % (removed_singletons_count)

    if removed_singletons_count == 0 : return # don't wirte new JSON without singleton_coords if there weren't singleton_coords removed

    new_biom_filename = biom_filename[0:biom_filename.index('.biom.json')] + '.no_singletons.biom.json'

    simplejson.dump(biom_data, open(new_biom_filename,'w'), indent=3)

#    commons.write_list_to_textfile( [ str(coord[0]) for coord in non_singleton_coords] , 'ids.txt'  )

    print "The filtered BIOM without singletons was sucessfully saved to", new_biom_filename
 #   commons.dump( biom_data)


if __name__ == '__main__':
    main()
