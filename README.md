# sub16s : 16S Pipelines Comparator Tool

## The purpose

**sub16s** is a [16S rRNA](https://en.wikipedia.org/wiki/16S_ribosomal_RNA) gene pipeline analyzer . The 16S rRNA is a marker gene used to identify prokaryotic species. The [NGS](https://en.wikipedia.org/w/index.php?title=Next-generation_sequencing) platforms, like the [Illumina MiSeq](http://www.illumina.com/systems/miseq.html) create datasets from reading amplicons targeting the 16S gene, which are specific segments of the genome containing only this gene, which are amplified through [PCR](https://en.wikipedia.org/wiki/Polymerase_chain_reaction).

The most-cited 16S gene pipelines include [mothur](http://www.mothur.org), [QIIME](http://www.qiime.org) and [USEARCH](http://drive5.com) which are capable of reading these datasets and build OTUs (Operational Taxonomic units) using clustering methods.

The most representative sequence for these OTUs are compared with already known sequences compiled into databases like SILVA, greengenes or RDP to identify the taxon to which they belong.

Our tool uses the BIOM files outputted by these pipelines which contains a summary of the composition in terms of taxa present in the datasets and compares the contents with an already known composition (since the datasets come from a mock community). The comparison is presented in terms of Unifrac similarity. The tool is able to use different merging results coming from different mergers, which are used as input to the pipelines, being tested different configurations in terms of merger/pipeline/reference.

## Getting started

Before starting the analyzer, the data directory must be created obeying the structure described below.

### Tree structure

The datasets can be put anywhere, even in a subdir inside the `sub16s` tree.

However, this dataset dir, let's call it [sub16s-data] must obey the following structure:

```
~~~~~~~~~~~~~~~~~~~
> +sub16s-data
> |-----dataset1
> |         +--merger1
> |          |     +-dataset1-merger1-MERGED.fastq
> |         +--merger2
> |          |     +-dataset1-merger2-MERGED.fastq
> |         :
> |         +--mergerN
> |               +-dataset1-mergerN-MERGED.fastq
> |-----dataset2
> |         +--merger1
> |         |     + dataset2-merger1-MERGED.fastq
> |         +--merger2
> |         |     + dataset2-merger2-MERGED.fastq
> |         :
> |         +--mergerN
> |               +-dataset2-mergerN-MERGED.fastq
> :
> +-----datasetN
>           +--merger1
>           |     + datasetN-merger1-MERGED.fastq
>           +--merger2
>           |     + datasetN-merger2-MERGED.fastq
>           :
>           +--mergerN
>                 +-datasetN-mergerN-MERGED.fastq
~~~~~~~~~~~~~~~~~~~
```

As you see, each FASTQ coming from each merger should be put in its own dir, named after the merger. Each pipeline will generate their own files inside this dir, organized in subdirs with the same name as the pipeline. This way, we keep everything in its proper place, avoiding mixing, which allows for easy cleaning, in case we want to start everything all over again.

### Dependencies

In terms of languages platform dependencies, those are:

- JDK 1.8
- Python 2.7.6

sub16s is dependent of two other projects, which were chosen to be versioned independently, namely:

- [UnifracSim](https://bitbucket.org/sviana/unifracsim.git)
- [mothur-wrapper](https://github.com/digfish/mothur-wrapper.git)

### Installing the pipeline software themselves

As stated above, you'll need to install mothur, QIIME and USEARCH.

We advise to install QIIME in the virtualenv created by the  `install.sh` script running `pip install numpy && pip install qiime`.

In [mothur's Github repository](https://github.com/mothur/mothur/releases) are available premade binary versions for each major OS .

USEARCH is free to [download](http://www.drive5.com/usearch/download.html) in its 32-bit flavored version, but some license restrictions apply and you'll need to provide an e-mail address in order to be able to download it.

The mothur and usearch binaries should be added to the `PATH`, and their names should be modified to `mothur` and `usearch` exactly. Otherwise, the analyzer will not be able to find them.

The versions used for each pipeline are:

pipeline | version
-------- | --------
QIIME    | 1.9.1
mothur   | 1.37.2
usearch  | 8.1.1861

### Pipelines references

Besides the code dependencies, you will need to download the OTU references for each pipeline. These are:

reference                                                  | version used
---------------------------------------------------------- | -------------------
[RDP](http://rdp.cme.msu.edu/misc/resources.jsp)           | release 11 update 4
[SILVA](https://www.arb-silva.de/download/archive/)        | 123
[Greengenes](http://greengenes.secondgenome.com/downloads) | 2013-8

QIIME if installed through `pip` will automatically download the greengenes reference with it. mothur and USEARCH don't come with the references by default, you'll have to manually download them. As an alternative, the install script will automatically download an archive file containing all the reference files from AFS.

## Setting everything up

### Install script

Stated everything above, all you need to do is run the `install.sh` script from the command line. This script must be **sourced**, not **executed**, i.e., the invocation must be this way:

```bash
. ./install.sh
```

and not:

```bash
./install.sh
```

This script will automatically perform all these steps:

- create a Python virtual env, switching to it imediately
- it will create the specific config file for the machine where it is running,
- will clone all the dependencies repo's locally
- set all the 16s referencies if they are already installed locally
- If QIIME is installed, it will search for the greengenes default reference and configure everything else.
- If you are inside the IGC network, it will download a bzip'd tarball containing all the references already formatted for each pipeline.

If something gets wrong on the 16S references downloads, you can repeat these step issuing the command

```bash
. ./setup_16s_refs.sh
```

### Example of invocation:

You want to test the application imediately, you can download a dataset ready to be used:

```bash
download_test_dataset.sh
```

Which will download the `Pos_CTL` dataset.

If until here, nothing strange happened, you can now do:

```bash
analyzer.sh Pos_CTL
```

will launch all pipelines in series, looping through all the references and folder `Pos_CTL-biom` will be created are is to where the resulting biom files generated by each pipeline will be copied to.

Then with the following command:

```bash
posprocessor.sh Pos_CTL
```

will clean, parse all the BIOM files, extract the taxons from it and then finally invoke the UnifracSim to calculate all the values which will be stored in the `Pos_CTL.weighted.unifrac.cleaned.tsv`. This is the file you want.

If you want to perform the previous steps with a single command, this what you want:

```bash
sub16s.sh Pos_CTL
```

## Sending emails

The entire pipeling running and its posterior processing can be take some time to complete. In case you pretend to be notified when the work is complete change the environment variable `$SUB16S_SEND_EMAIL` in the hostconfig file created automatically by the install script inside the `hostconfigs` dir and change the subsequent variables to reflect the desired values.

## Scripts index

name                          | What it does
----------------------------- | -----------------------------------------------------------------------
install.sh                    | install script
init-env.sh                   | Initialize the virtual environment used by Python
analyzer.sh                   | Main script, invokes all the pipelines in a dataset
posprocess.sh                 | All downstream work after the BIOM files are ready
sub16s.sh                     | The whole process, from beginning to end: Invokes the two scripts above
setup_16s_refs.sh             | Downloads and config the 16S gene references
clear.sh                      | removes all the files generated by the pipelines
retrieve_16s_refs_from_afs.sh | download a ready to be used archive containing all 16S references
create-new-hostconfig.sh      | create a new config specific for some host
