#!/bin/bash
set -x
PIPELINE=$1
DATASET=$2
#BIOM_OUTDIR=$PWD/$DATASET-biom
for fastq in $DATASET/*/*.fastq ; do
	echo -e "Running $PIPELINE over $fastq"
	CUR_DIR=`dirname $fastq` \
 	BIOM_OUTDIR=$PWD/$DATASET-biom \
 	$PIPELINE-run-them-all.sh $PWD/$fastq
 	 
done
