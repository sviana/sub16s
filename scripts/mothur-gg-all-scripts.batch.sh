#!/bin/bash

## batch script for mothur using greengenes
FASTQ=$1
STRIPPED=`mstripext.py $FASTQ`
RADIX=`basename $STRIPPED`

MOTHUR_OUT_DIR=$CUR_DIR/mothur_gg
mkdir -p $MOTHUR_OUT_DIR
MOTHUR_GG_DB_DIR=$MOTHUR_DB_DIR/gg


$MOTHUR $SUB16S_ROOT_DIR/pipelines-scripts/script1.mothur.batch \
	--fastq $FASTQ \
	--output_dir $MOTHUR_OUT_DIR \
	--reference $MOTHUR_GG_DB_DIR/gg.refalign.fasta

$MOTHUR $SUB16S_ROOT_DIR/pipelines-scripts/script2.after_align.mothur.batch \
	--output_dir $MOTHUR_OUT_DIR \
	--start 2264 --end 4051 


$MOTHUR $SUB16S_ROOT_DIR/pipelines-scripts/script3.after_filter_seqs.mothur.batch \
	--output_dir $MOTHUR_OUT_DIR \
	--reference $MOTHUR_GG_DB_DIR/gg_otus.fasta \
	--taxonomy $MOTHUR_GG_DB_DIR/gg.tax

LAST_CONSTAXONOMY=`ls -t $MOTHUR_OUT_DIR/*.cons.taxonomy | head -n1`

$MOTHUR $SUB16S_ROOT_DIR/pipelines-scripts/script4.after_classify-otu.mothur.batch \
	--output_dir $MOTHUR_OUT_DIR \
	--constaxonomy $LAST_CONSTAXONOMY

#copy the resulting biom file(s) to the biom dir
#cp -v `ls -t $MOTHUR_OUT_DIR/*.biom` $INITIAL_DIR/biom/

cp -v $MOTHUR_OUT_DIR/*.biom $BIOM_OUTDIR/$RADIX.mothur.gg.biom.json
