#!/bin/bash

rm $PWD/.mothur-wrapper.ini

FASTQ=$1

# if [ ! -v MOTHUR_DB_DIR ] ; then
# 	MOTHUR_DB_DIR=$HOME/work/MOTHUR_DB_DIR
# 	export MOTDUR_DB_DIR
# fi
set -x
if [ ! -v CUR_DIR ] ; then
	CUR_DIR=`dirname $FASTQ`
	export CUR_DIR
fi

MOTHUR="mothur-wrapper.py -v"
export MOTHUR

echo -e ">>> Executing the mothur pipeline with the 3 references on $FASTQ"

rm $INITIAL_DIR/.mothur-wrapper.ini

# cycle mothur through every reference

MOTHUR_REFS=$MOTHUR_DB_DIR/*

for ref in $MOTHUR_REFS  ; do
	ref=`basename $ref`
	echo $ref
	REF=`echo $ref | tr '[:lower:]' '[:upper:]'`
	echo -e ">>> Running mothur using $REF over $FASTQ"
	. mothur-$ref-all-scripts.batch.sh $FASTQ
	rm $INITIAL_DIR/.mothur-wrapper.ini
done

set +x
