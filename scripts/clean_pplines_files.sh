#!/bin/bash

## ```clean_pplines_files.sh```
##  will clean all the pipelines generated files in a merger subdir from the whole dataset
## useful to restart everything from scratch, being sure no file stays in the way that could interfere
## with the entire process of running every pipeline

FASTQ_DIR=$1

echo -e "Will clean pipelines generated files on $FASTQ_DIR"

for dir in $FASTQ_DIR/*/ ;
	do
		echo -e "Removing $dir" ;
		rm -rv $dir
done

rm -v $FASTQ_DIR/sub16s.log
