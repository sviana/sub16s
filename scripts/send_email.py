#!/usr/bin/env python
import sys
import smtplib as smtp
import argparse
import fileinput
import email.utils
from email.mime.text import MIMEText

def main():

    argparser = argparse.ArgumentParser(description='sends an e-mail with params from the command line')

    argparser.add_argument('--sender_email',help='The  sender\'s email address')
    argparser.add_argument('--sender_name',help='The  sender\'s name')
    argparser.add_argument('--to',help='The recipient\'s email address')
    argparser.add_argument('--subject',help='The message subject')

    args = argparser.parse_args()


    print "Please enter text. When completed press CTRL+D:"
    
    text =  "".join(sys.stdin)

    print text

    msg = MIMEText(text)
    msg['From'] = email.utils.formataddr( (args.sender_name, args.sender_email) )
    msg['To'] = email.utils.formataddr( ('Recipient', args.to) )
    msg['Subject'] =  args.subject


    # Send the message via our own SMTP server, but don't include the
    # envelope header.
    s = smtp.SMTP('mail.igc.gulbenkian.pt')
    s.set_debuglevel(False)

    try:
        s.sendmail(args.sender_email, args.to, msg.as_string() )
    finally:
        s.quit()


if __name__ == '__main__':
    main()
