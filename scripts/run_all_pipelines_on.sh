#!/bin/bash

## invokes all the pipelines in some  DATASET/MERGER dir
## the args are:
## 1. the pair DATASET/MERGER as: dataset1/merger1
## 2. the BIOM dir where the final *.biom files should be copied to
## this script are called by [sub16s.sh]
## Usage example:
##		./run_all_pipelines_on.sh Pos_CTL/PEAR Pos_CTL-biom

if [ -z $1 ] ; then
	echo "Needs a location of a dataset/merged dir where is a merged fastq !"
	exit 0
fi

. scripts/functions.sh

#initializes the environment if it was no setted before
. init-env.sh

DATASET_MERGER_PAIR=$1
BIOM_OUTDIRNAME=$2

echo -e ">>> Will run on $DATASET_MERGER_PAIR"

LOGFILE=$DATASET_MERGER_PAIR/sub16s.log

INITIAL_DIR=$PWD

CUR_DIR=$INITIAL_DIR/$DATASET_MERGER_PAIR

#. init-env.sh

# search for the FASTQ inside the subdir

FASTQ=`find $CUR_DIR/ -name '*.fastq' `

echo -e "fastq found for processing: $FASTQ"


#empty previous log file
echo "" > $LOGFILE

echo "Saving all output to $LOGFILE ..."
exec 2>&1
exec &> >(tee -a "$LOGFILE")



if [ ! -v BIOM_OUTDIR ]; then
	BIOM_OUTDIR=$INITIAL_DIR/$BIOM_OUTDIRNAME
	export BIOM_OUTDIR
fi

echo -e "The biom files will be stored in $BIOM_OUTDIR"
mkdir -p $BIOM_OUTDIR


echo -e ">>> Running all pipelines on $CUR_DIR"


### ---QIIME -----
if [ $(is_on_path print_qiime_config.py) -eq '1' ] ; then
	. qiime-run-them-all.sh $FASTQ
else
  echo " !!!!!!! COMMAND NOT FOUND: qiime !!!!!!"
fi



### --- MOTHUR ---

if [ $(is_on_path mothur) -eq '1' ] ; then
		cd $INITIAL_DIR
		. mothur-run-them-all.sh $FASTQ
else
  echo "!!!!!!! COMMAND NOT FOUND: mothur !!!!!!!"
fi



### --- USEARCH ---

if [ $(is_on_path usearch) -eq '1' ] ; then
	. usearch-run-them-all.sh $FASTQ
else
  echo "!!!!!!! COMMAND NOT FOUND: usearch !!!!!!!"
fi




#send an email notifying about the conclusion
#cat $INITIAL_DIR/$LOGFILE | send_email.py \
#	--sender_email "sviana@igc.gulbenkian.pt"\
#	--sender_name "sub16s pipelines tester"\
#	--subject "The all pipelines script running over $DATASET_MERGER_PAIR completed its work!"\
#	--to "pescadordigital@gmail.com"

cd $INITIAL_DIR

echo -e "##### TERMINATED ANALYSIS on $DATASET_MERGER_PAIR ######"

echo $PWD
