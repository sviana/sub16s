#!/bin/bash
BIOM_OUTDIR=$1
QTYS_OUTDIR=$2
QTYS_CONTROL_FILE=$3


if [ ! -v DATASET ] ; then
	DATASET=`echo $BIOM_OUTDIR | cut -d- -f1`
fi


#extract the qtys from each BIOM, writing a .qtys.tsv file for each BIOM

for biom in $BIOM_OUTDIR/*.biom.json; do
	biom_extract_qtys.py $biom
done

#create the directory

mkdir -p $QTYS_OUTDIR

#copy all the .qtys.tsv files to a new dir where they will be processed
# by the Similarity Calculator

cp -v $BIOM_OUTDIR/*.qtys.tsv $QTYS_OUTDIR

if [ -e $UNIFRAC_SIM_DIR/unifracsim.jar ] ; then
	java -jar $UNIFRAC_SIM_DIR/unifracsim.jar  -d $PWD/$QTYS_OUTDIR \
		-c $UNIFRAC_SIM_DIR/$NAMES_CONTROL_FILE -t $UNIFRAC_SIM_DIR/ncbitaxon.obo -q
else
	ant -f $UNIFRAC_SIM_DIR/build.xml  unweighted_on_dir -Dnames_dir=$PWD/$QTYS_OUTDIR \
		-Dcontrol=$NAMES_CONTROL_FILE
fi
if [ ! -v DATASET ] ; then
	DATASET=`echo $BIOM_OUTDIR | cut -d- -f1`
fi


python $SUB16S_SCRIPTS_DIR/clean_sim_tsv.py  $DATASET.weighted.unifrac.tsv
