#!/bin/bash

## invoke the QIIME scripts with the FASTQ
## as its sole argument
## assumes the BIOM_OUTDIR env var are already setted in the context
## the mstripext.py is a Python executable from the mypycommons project
## this script is one entry point to run all the whole of the QIIME pipeline
## with every reference (Greengenes, SILVA and RDP)

FASTQ=$1
STRIPPED=`mstripext.py $FASTQ`
RADIX=`basename $STRIPPED`

QIIME='qiime'
QIIME_OUT_DIR=$CUR_DIR/qiime
mkdir -p $QIIME_OUT_DIR
cd $QIIME_OUT_DIR


echo -e ">>> Running QIIME over $FASTQ\n\n"

python $SUB16S_SCRIPTS_DIR/qiime-generate-mapping-file-for-fastq.py $FASTQ

. $PIPELINES_SCRIPTS_DIR/qiime-fastq_first_step.bash $FASTQ $STRIPPED.map.txt

QIIME_REFS=`ls $QIIME_DB_DIR`

for ref in $QIIME_REFS ; do

	echo -e ">>> Now picking OTUS using $ref\n"

	. $PIPELINES_SCRIPTS_DIR/qiime-pick_and_plot.bash \
		$QIIME_OUT_DIR/slout/seqs.fna \
		$STRIPPED.map.txt \
		$ref \
		$SUB16S_SCRIPTS_DIR/qiime_plot_taxa_params.txt

	cp -v $RADIX.qiime.$ref.biom.json  \
		$BIOM_OUTDIR
done
