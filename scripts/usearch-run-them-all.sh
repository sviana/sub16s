#!/bin/bash
set -x

FASTQ=$1
#echo -e "NUM_CPUS=$NUM_CPUS"

#if [ ! -v CUR_DIR] ; then
#	CUR_DIR=`dirname $FASTQ`
#	export CUR_DIR
#fi

echo -e "CUR_DIR=$CUR_DIR"


USEARCH="usearch -threads $NUM_CPUS"
USEARCH_OUT_DIR=$CUR_DIR/usearch

echo -e "\n-------\n>>> Now running USEARCH over $FASTQ\n-------\n"

mkdir -p $USEARCH_OUT_DIR
cd $USEARCH_OUT_DIR


. $PIPELINES_SCRIPTS_DIR/usearch-script1.bash $FASTQ

USEARCH_REFS=$USEARCH_DB_DIR/*

for db in $USEARCH_REFS ; do
. $PIPELINES_SCRIPTS_DIR/usearch-script2.bash $FASTQ `basename $db`
done
set +x
