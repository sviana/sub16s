import sys

from Bio import SeqIO
import commons


def main():
    fastq_filename = sys.argv[1]

    cleaned_fastq_filename = commons.basename(fastq_filename) + ".cleaned." + commons.extension(fastq_filename)

    cleaned_fastq = open(cleaned_fastq_filename, "w")

    for rec in SeqIO.parse(fastq_filename, commons.extension(fastq_filename)):
        new_id = rec.id.replace('_', ':')
        rec.id = new_id
        rec.description = new_id
        SeqIO.write(rec,cleaned_fastq,'fastq')




if __name__ == '__main__':
    main()
