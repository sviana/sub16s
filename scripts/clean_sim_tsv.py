#!/usr/bin/env python
'''
@summary:
Cleans the TSV with the unifrac data calculated by UnifracSim from the
rows generated with singletons, besides sorting all the entries alphabetically
Created on Apr 25, 2016
Example:

```
    clean_sim_tsv.py dataset1.unifrac.weighted.tsv
```

will generate a dataset.unifrac.weighted.clean.tsv file in the same dir

@author: sam
'''
import os,sys
import commons
import csv
import re

def filter_mothur_out(term):
    """ callback function

    used in a filter to remove the rows related to mothur where the singletons were not removed
    """
    tokens = term[0].split('.')
    dataset = tokens[0]
    merger = tokens[1]
    pipeline = tokens[2]
    if len(tokens) > 3:
        bd = tokens[3]
    return pipeline != 'mothur' or (pipeline == 'mothur' and len(tokens) == 5)


def main():
    """main function

    receives the TSV filename, cleans it from the uneeded rows and writes a new TSV with rows
    ordered by the terms in the first column
    """
    tsv_filename = sys.argv[1]
    tsv = open(tsv_filename,"r")
    unifrac_data = list()
    headers = tsv.readline().strip('\"\n').split('\t')
#    print headers
    for row in tsv.readlines():
        unifrac_row = row.strip('\"\n').split('\t')
        unifrac_row[0] = unifrac_row[0].split("/")[-1]
#        commons.dump(unifrac_row)
        unifrac_data.append(unifrac_row)

    #print headers
    #commons.dump(unifrac_data)
    unifrac_data = filter(filter_mothur_out , unifrac_data )

    #print unifrac_data
    sorted_unifrac_data = sorted(unifrac_data,key=lambda item: item[0])
    commons.dump(sorted_unifrac_data)
    writer = csv.writer(open(commons.basename(tsv_filename) + ".cleaned.tsv","w"),delimiter='\t',quoting=csv.QUOTE_NONE)
    writer.writerow(headers)
    for row in sorted_unifrac_data:
        writer.writerow(row)


if __name__ == '__main__':
    main()

