#!/bin/bash
set -x
BIOM_OUTDIR=$1
NAMES_OUTDIR=$2
NAMES_CONTROL_FILE=$3

if [ ! -v DATASET ] ; then
	DATASET=`echo $BIOM_OUTDIR | cut -d- -f1`
fi

#extract the names from each BIOM, writing a .names.txt file for each BIOM

for biom in $BIOM_OUTDIR/*.biom.json; do
	biom_extract_names.py $biom
done

#create the directory

mkdir -p $NAMES_OUTDIR

#copy all the .names.txt files to a new dir where they will be processed
# by the Similarity Calculator

cp -v $BIOM_OUTDIR/*.names.txt $NAMES_OUTDIR

if [ -e $UNIFRAC_SIM_DIR/unifracsim.jar ] ; then
	java -jar $UNIFRAC_SIM_DIR/unifracsim.jar  -d $PWD/$NAMES_OUTDIR -c $UNIFRAC_SIM_DIR/$NAMES_CONTROL_FILE \
		-t $UNIFRAC_SIM_DIR/ncbitaxon.obo
else
	ant -f $UNIFRAC_SIM_DIR/build.xml  unweighted_on_dir -Dnames_dir=$PWD/$NAMES_OUTDIR -Dcontrol=$NAMES_CONTROL_FILE
fi

python $SUB16S_SCRIPTS_DIR/clean_sim_tsv.py  $DATASET.unweighted.unifrac.tsv
