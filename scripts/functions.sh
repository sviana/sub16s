#!/bin/bash

function is_on_path {
  out=`which $1`
  if [ ${#out} -eq 0 ]; then
    echo 0
  else
    echo 1
  fi

}
