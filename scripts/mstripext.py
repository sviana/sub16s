#!/usr/bin/env python
import os,sys

#strips the extensions out of the filename, assumes the dot (".") is used as the delimiter
#assumes -1 as the default, it will strip the final token only
#final_pos should always be negative
def mstripext(filename,final_pos=-1):
    filename_tokens = filename.split('.')
    fasta_basename = ".".join(filename_tokens[0:len(filename_tokens) + int(final_pos)])
    return fasta_basename


if len(sys.argv) == 2:
    print mstripext(sys.argv[1])
else:
    print mstripext(sys.argv[1], sys.argv[2])
