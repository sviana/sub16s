#!/usr/bin/env python
import argparse
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
import email.utils
import fileinput
from os.path import basename
import sys

import smtplib as smtp
from email.mime.multipart import MIMEMultipart


def main():

    argparser = argparse.ArgumentParser(description='sends an e-mail with params from the command line')

    argparser.add_argument('--sender_email',help='The  sender\'s email address')
    argparser.add_argument('--sender_name',help='The  sender\'s name')
    argparser.add_argument('--to',help='The recipient\'s email address')
    argparser.add_argument('--subject',help='The message subject')
    argparser.add_argument('--attach_file',help='The name of the file to attach')
    argparser.add_argument('--smtp_server',help='The SMTP server to send the message through')

    args = argparser.parse_args()


    print "Please enter text. When completed press CTRL+D:"

    text =  "".join(sys.stdin)

    print text



    msg = MIMEMultipart("")

    msg.attach(MIMEText(text))
    if args.attach_file:
        attach_filename = args.attach_file
        msg.attach(MIMEApplication(
            open(attach_filename,"rb").read(),
            Content_Disposition='attachment; filename="%s"' % basename(attach_filename),
            Name=basename(attach_filename)
        ))

    msg['From'] = email.utils.formataddr( (args.sender_name, args.sender_email) )
    msg['To'] = email.utils.formataddr( ('Recipient', args.to) )
    msg['Subject'] =  args.subject


    # Send the message via some SMTP server, but don't include the
    # envelope header.
    s = smtp.SMTP(args.smtp_server)
    s.set_debuglevel(False)

    try:
        s.sendmail(args.sender_email, args.to, msg.as_string() )
    finally:
        s.quit()


if __name__ == '__main__':
    main()
