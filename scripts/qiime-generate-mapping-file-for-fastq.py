import os, sys
from os import path


def main():
    fastq_filename = sys.argv[1]

    if len(sys.argv) > 2:
        new_mapping_file_name = sys.argv[2]
    else:
        new_mapping_file_name = ".".join(fastq_filename.split(".")[:-1]) + ".map.txt"



    mapping_file_header = ['#SampleID','BarcodeSequence','LinkerPrimerSequence','Description','File']
    mapping_file_line1 = ['sample.1','','','single_file',path.basename(fastq_filename)]


    with open(new_mapping_file_name,"w") as f:
        f.write("\t".join(mapping_file_header) + "\n")
        f.write("\t".join(mapping_file_line1) + "\n")


    print "New mapping file is ", new_mapping_file_name

if __name__ == '__main__':
    main()
