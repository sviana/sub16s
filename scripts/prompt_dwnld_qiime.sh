#/bin/bash

echo "Do you want to install QIIME on the new virtualenv ?  (it will take time!!)"
read  -e -p "Enter y if you intend to do that: " prompt
if [ "$prompt" == "y" ] ; then
  echo "Going to install QIIME"
  pip install numpy
  pip install qiime 
else
  echo -e "QIIME is not going to be installed! You can do that later in the virtualenv through: \n 
  pip install numpy && pip install qiime\n"
fi
