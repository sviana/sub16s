#!/bin/bash

sed -e "s/\$HOSTNAME/$HOSTNAME/g" \
 templates/hostname.config.tmpl > hostconfigs/`hostname`.config
