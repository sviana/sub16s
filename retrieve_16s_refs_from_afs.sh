#!/bin/bash
# the 16s_refs tarball were copied to
#/afs/igc.gulbenkian.pt/folders/UBI/PROJECTS/SVIANA/16s_refs.tar.bz2
set -x
if [ ! -e 16s_refs.tar.bz2 ] ; then
	echo "Enter your AFS password in order to download an archive containing all the 16S references (kinit is invoked without params and the password is not stored)"
	kinit
	cp -v /afs/igc.gulbenkian.pt/folders/UBI/PROJECTS/SVIANA/16s_refs.tar.bz2 .
fi
tar --keep-old-files -jxvf 16s_refs.tar.bz2
set +x
