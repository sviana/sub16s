#!/bin/bash
FASTA=$1
MAP_FILE=$2
REF_NAME=$3
PLOT_TAXA_PARAMS=$4
set -x
#QIIME_DB_DIR=$HOME/work/qiime_db

OUT_DIR="open_ref_otus.$REF_NAME"

REF_DB_PARAMS="$SUB16S_ROOT_DIR/qiime-confs/qiime-config-use-$REF_NAME.conf"

echo -e "Using $REF_DB_PARAMS as config file for reference"

pick_open_reference_otus.py \
	-o $OUT_DIR/ \
	-i $FASTA \
	--parameter_fp $REF_DB_PARAMS \
	-r $QIIME_DB_DIR/$REF_NAME/"$REF_NAME"_otus.fasta \
	-f

biom convert \
	-i $OUT_DIR/otu_table_mc2_w_tax_no_pynast_failures.biom \
	-o $RADIX.qiime.$REF_NAME.biom.json \
	--to-json \
	--table-type="OTU table"
