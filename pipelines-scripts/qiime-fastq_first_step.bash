#!/bin/bash
FASTQ=$1
MAP_FILE=$2
PLOT_TAXA_PARAMS='taxa_params.txt'
#set -x

 split_libraries_fastq.py \
 	-i $FASTQ\
 	-o slout\
 	--barcode_type 'not-barcoded'\
 	-m $MAP_FILE\
 	--sample_ids sample.1\
 	--phred_offset 33

count_seqs.py -i slout/seqs.fna

