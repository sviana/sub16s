#!/bin/bash
U=$USEARCH
#USEARCH_DB_DIR=$HOME/work/usearch_db
UDB_TAX=$USEARCH_DB_DIR/$UDB_NAME.udb
MERGED_FASTQ=$1
BASENAME=`basename $MERGED_FASTQ`
RADIX=`mstripext.py $BASENAME`

#merge the paired-end reads (not needed, just for reference, already done!)
#$U -fastq_mergepairs $forw -reverse $rev -fastqout $out

echo -e "PWD is $PWD"

#filter dataset
$U -fastq_filter $MERGED_FASTQ -fastq_maxee 100.0 -relabel Filt -fastaout $RADIX.fasta

#remove repeated sequences
$U -derep_fulllength $RADIX.fasta -relabel Uniq -sizeout -fastaout $RADIX.uniques.fasta

#fetch OTUs
$U -cluster_otus $RADIX.uniques.fasta -minsize 2 -otus $RADIX.uniques.otus.fasta \
 -relabel Otu

