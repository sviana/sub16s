#!/bin/bash
U=$USEARCH
#USEARCH_DB_DIR=$HOME/work/usearch_db
UDB_NAME=$2
UDB_TAX=$USEARCH_DB_DIR/$UDB_NAME/$UDB_NAME.udb
FASTQ=$1

BASENAME=`basename $FASTQ`
RADIX=`mstripext.py $BASENAME`.uniques.otus
MERGED_DATASET_NAME=`mstripext.py $BASENAME -1`

#assign taxonomy
$U -utax $RADIX.fasta -db $UDB_TAX -utaxout $RADIX.utax -strand both \
	-fastaout $RADIX.with_tax.fasta  #-alnout $1.aln.txt

#with the taxonomy assigned, search using the OTUs found as the reference
$U -usearch_global $FASTQ -db $RADIX.with_tax.fasta -strand plus -id 0.97 \
  -otutabout $RADIX.otutab.txt -biomout $MERGED_DATASET_NAME.usearch.$UDB_NAME.biom.json \
  -mothur_shared_out $MERGED_DATASET_NAME.usearch.$UDB_NAME.otutab.shared

#copy resulting biom to the final dir
cp -v $MERGED_DATASET_NAME.usearch.$UDB_NAME.biom.json $BIOM_OUTDIR

