from functools import partial
import pprint
import subprocess
from collections import OrderedDict, Counter
import os
import sys
import glob
import csv
import pickle
import multiprocessing as mp
import time

from orderedset import OrderedSet
from Bio import SeqIO
from Bio.Seq import Seq

__author__ = 'sam'

def show(str):
    """Write a string to the console without a newline at the end"""
    sys.stdout.write(str)


def count_size_megs(object):
    """displays the size ocupied by the object in memory in megabytes"""
    mem_megs = int(sys.getsizeof(object)) / float(1024*1024)
    return mem_megs


def extension(filename):
    """returns the extension of the filename"""
    return filename.split('.')[-1]


def count_lines(filename):
    """Returns the number of lines in the file named filename"""
    output = subprocess.check_output( "wc -l %s" % (filename), shell=True)
    return int(output.split(" ")[0])

def exec_output(some_command):
    """executes and returns the output of some shell program"""
    output = subprocess.check_output( some_command, shell=True)
    return output


def list_files(directory,filter="*"):
    """list the files in the directory, specifiying a filter"""
    return [ f for f in glob.glob( directory + "/"+  filter) ]


def extract_token_ids(filenames):
    token_ids = list()
    for f in filenames:
        token_ids.append( get_filename_token_id(f) )
    return token_ids

def trim_accession(full):
#    print full
    tokens = full.split(':')
    s = len( tokens )
    result = ":".join(tokens[s-3:s])
#    print result
    return result

def get_filename_token_id(filename):
    return filename.split('.')[1]


def fetch_table_dict_headers(dict):
    """given a data-frame (dictionary with dictionary as rows, return the keys of the dictionary row"""
    if len(dict.keys()) > 0  and  len (dict[dict.keys()[0]].keys()) > 0:
        return dict[dict.keys()[0]].keys()
    else: return None



def basename(filename):
    filename_tokens = filename.split('.')
    fasta_basename = ".".join(filename_tokens[0:len(filename_tokens) - 1])
    return fasta_basename


def strip_extension(filename):
    return ".".join(filename.split('.')[:-1])




def fastq_to_fasta_p(fastq_filename):
    new_fasta_filename =  basename(fastq_filename) + ".fasta"
    cmdline = 'fastq_to_fasta -i ' + fastq_filename + ' -o %s -n' % (new_fasta_filename)

    executing(cmdline)
    return new_fasta_filename


def fastq_to_fasta(fastq_filename):
    new_fasta_filename = basename(fastq_filename) + ".fasta"
    new_fasta = open(new_fasta_filename,"w")
    for seq_record in SeqIO.parse( open(fastq_filename),'fastq'):
        SeqIO.write(seq_record, new_fasta,"fasta")
    return new_fasta_filename


def blast_one_sequence(seq_rec,blastdb,identity_threshold):
    subcmdline = "bash -c \"echo  '>" + seq_rec.name + "\n" + str( seq_rec.seq ) \
                 + "\n' | blastn  -query - -outfmt \'6 qacc qlen pident mismatch stitle staxids\' " \
                 + "-max_target_seqs 1 -perc_identity %s -db %s \"" % (identity_threshold,blastdb)
    return exec_output(subcmdline)

def run_blast(fasta, blastresults_filename, blast_args):

    subcmdline = "blastn  -query %s -outfmt 6" % (fasta)

    for (name,value) in blast_args.iteritems():
        subcmdline += " -%s %s" %(name,str(value))

    cmdline = subcmdline + "  >> %s " % (blastresults_filename)
    executing(cmdline, True)
    return blastresults_filename


def run_blast_parallel(fasta, blastresults_filename, blast_args):
    """ Run as blast in parallel over the fasta file as the source"""
    subcmdline = "blastn -outfmt 6"
    for (name,value) in blast_args.iteritems():
        subcmdline += " -%s %s" %(name,str(value))


    cmdline = "cat " + fasta + " | parallel -k --block 100k --recstart '>' --pipe " +\
              subcmdline + " -query - " +  " >> %s " % (blastresults_filename)
    executing(cmdline, True)
    return blastresults_filename


def run_blast_1by1_mp(fasta, ref_dir, accession_organism_table_filename, blastresults_filename, temp_dir, ref_type,
                      blast_args):
    initialcmdline = "blastn -outfmt 6 -query - "

    for (name, value) in blast_args.iteritems():
        initialcmdline += " -%s %s " % (name, str(value))

    print "Fetching table id/organism"

    # when using the tsv simple id/organism
    accession_organism_dict = tsv_fetch_rows(accession_organism_table_filename)

    all_seq_recs_dict = fetch_seq_records(fasta, 'fasta')

    print "Executing blast 1-by-1 over " + fasta

    run_blast_1by1_over_one_rec = partial(run_blast1by1_over_rec,
                                          accession_organism_dict=accession_organism_dict,
                                          ref_dir=ref_dir,
                                          initialcmdline=initialcmdline,
                                          ref_type=ref_type,
                                          blastresults_filename=blastresults_filename)

    #   run_blast_1by1_over_one_rec = partial(run_blast_mp_rec,blastresults_filename)
    pool = mp.Pool()
    pool.map(run_blast_1by1_over_one_rec, all_seq_recs_dict.values())
    pool.close()
    pool.join()


#### calback for one record ####

def run_blast1by1_over_rec(seq_rec, accession_organism_dict, ref_dir, initialcmdline, ref_type, blastresults_filename):
    accession = seq_rec.name

    try:
        organism = accession_organism_dict[seq_rec.name][1]

        if organism.find('|') > 0:
            organism = organism.split('|')[1]
    except KeyError:
        #        print "Id not found on organisms table : ", accession, ", skipping..."
        return

    organism_ref_path = ref_dir + os.sep + organism

    subcmdline = "echo  '>" + seq_rec.name + "\n" + str(seq_rec.seq) + "\n' | " + initialcmdline

    if ref_type == 'fasta':
        organism_fasta = organism_ref_path + ".fasta"
        subcmdline += "-subject %s " % organism_fasta

    elif ref_type == 'blastdb':
        organism_blastdb = organism_ref_path + ".blastdb"
        subcmdline += "-db %s " % organism_blastdb

    cmdline = subcmdline + " >> %s " % (blastresults_filename)
    executing(cmdline, False)


#### end of callback over one record #####






def run_blast_1by1(fasta, ref_dir, accession_organism_table_filename, blastresults_filename, temp_dir, ref_type,
                   blast_args):
    """
    :param fasta: the FASTA with the sequences to be blastted
    :param ref_dir: the dir with FASTA sequences with the organism sequences used as references
    :param accession_organism_table_filename: the tabular file with the relation accession/best organism
    :param blastresults_filename the file name where the blast results will be stored
    :param ref_type: the referent format (BLASTDB or FASTA)
    :param blast_args: any extra parameters to be passed to blast
    :return:
    """""

    print "Splitting original FASTA " + fasta + " . . . ."
    walk_sequences(fasta, write_single_seq_record, target_dir=temp_dir)

    print "concluded!"

    initialcmdline = "blastn -outfmt 6"
    for (name, value) in blast_args.iteritems():
        initialcmdline += " -%s %s " % (name, str(value))

    print "Fetching table id/organism"

    # when using the tsv simple id/organism
    accession_organism_dict = tsv_fetch_rows(accession_organism_table_filename)

    # when using the analyzed blastresults directly (analyzed blastresults file)
    # accession_organism_dict = tsv_fetch_rows(accession_organism_table_filename,0,get_proper_read_id, lambda row: row[1])

    seq_records = fetch_seq_records(fasta, 'fasta')

    print "Executing blast 1-by-1 over " + fasta

    for accession in seq_records:

        seq_rec = seq_records[accession]

        try:

            organism = accession_organism_dict[seq_rec.name][1]
            # organism = accession_organism_dict[seq_rec.name]

        except KeyError:

            print "Id not found on organisms table : ", accession, ", skipping..."
            continue



        seq_fasta_filename = temp_dir + os.sep + seq_rec.name + ".fasta"

        seq_fasta_filename = seq_fasta_filename.replace(':', '_')

        # seq_fasta = open(seq_fasta_filename,'w')

        # SeqIO.write(seq_rec,seq_fasta_filename,'fasta')



        organism_ref_path = ref_dir + os.sep + organism

        subcmdline = initialcmdline + " -query %s " % seq_fasta_filename

        if ref_type == 'FASTA':

            organism_fasta = organism_ref_path + ".fasta"

            subcmdline += "-subject %s " % organism_fasta

        else:

            organism_blastdb = organism_ref_path + ".blastdb"

            subcmdline += "-db %s " % organism_blastdb

        if not os.path.exists(seq_fasta_filename):
            print "Couldn't find " + seq_fasta_filename
            continue

        if not os.path.getsize(seq_fasta_filename) != 0:
            print "File " + seq_fasta_filename + " is empty!"
            continue

        cmdline = subcmdline + " >> %s " % (blastresults_filename)

        executing(cmdline, False)

        os.remove(seq_fasta_filename)

    print "Removing all single-read FASTAS...."

    rm_all_files_in_dir(temp_dir)

    print "DONE!"


def run_blast_mp(fasta, blastresults_filename, extra_blast_args):
    from functools import partial
    run_blast_sequence = partial(run_blast_mp_rec, blastresultsfilename=blastresults_filename,
                                 blast_args=extra_blast_args)
    seq_recs = SeqIO.parse(fasta, 'fasta')
    pool = mp.Pool()
    pool.map(run_blast_sequence, seq_recs, 1)
    pool.close()
    pool.join()


def run_blast_mp_rec(seq_rec, blastresultsfilename, blast_args):
    subcmdline = "echo  '>" + seq_rec.name + "\n" + str(seq_rec.seq) + "\n' | blastn  -query - -outfmt 6 "

    for (name, value) in blast_args.iteritems():
        subcmdline += " -%s %s" % (name, str(value))

    cmdline = subcmdline + "  >> " + blastresultsfilename
    executing(cmdline, False)



def rm_all_files_in_dir(temp_dir):
    for file in list_files(temp_dir):
        os.remove(file)

def fetch_sequences_into_ordered_dict(reads_file,fileformat = None):
    """Fetch all the sequences from file and insert them into a ordered dict, preserving this
    way their original sorting"""
    if fileformat == None:
        fileformat = reads_file.split('.')[-1]

    reads = OrderedDict()
    for seqRecord in SeqIO.parse(reads_file,fileformat) :
        reads[ seqRecord.name ] =  str(seqRecord.seq)
    #print reads

    print  "Number of sequences in " + reads_file + ":" +  str(len(reads))
    return reads

def fetch_seq_records(reads_file,fileformat):
    reads = OrderedDict()
    for seqRecord in SeqIO.parse(reads_file,fileformat) :
        reads[ seqRecord.name ] =  seqRecord
    return reads

def fetch_seq_records_trim_format(reads_file,fileformat):
    reads = OrderedDict()
    for seqRecord in SeqIO.parse(reads_file,fileformat) :
        reads[ trim_accession(seqRecord.name) ] =  seqRecord
    return reads

def write_list_fasta(filename, sequences_dict_list):
    """ write a list with a dictionary with keys 'name' and 'data' on a FASTA file"""
    f = open(filename, "w")
    for item in sequences_dict_list:
        f.write(">" + item['name'] + "\n" + item['data'] + "\n")


def write_dict_fastq(filename, seq_recs):
    """write a dictionary with fastq seqRecord's to fastq"""
    f = open(filename, 'w')
    seq_recs_list = map(lambda item: item['rec'], seq_recs.values())
    SeqIO.write(seq_recs_list, f, 'fastq')

def write_seq_in_fasta_without_newlines(fastaHandle,seqRecord):
    fastaHandle.write('>' + seqRecord.id + "\n" + str(seqRecord.seq) + "\n")

def write_bare_fasta_without_newlines(fastaHandle,seq_id,sequence):
    fastaHandle.write('>' + seq_id + "\n" + sequence + "\n")

def fetch_accessions(reads_file,format=None,trim_acc=False):

    if format is None:
        format = reads_file.split('.')[-1]

    accessions = OrderedSet()
    if trim_acc:
        for seqRecord in SeqIO.parse(reads_file,format) :
            accessions.add( trim_accession(seqRecord.name) )

    else:
        for seqRecord in SeqIO.parse(reads_file,format) :
            accessions.add( seqRecord.name )
    return accessions


def recent_mfiles(dir):
    return exec_output('ls -t ' + dir + ' | head -n5').split('\n')


def recent_mfile(dir):
    return recent_mfiles(dir)[0]


def fastx_count_records(fastx_file, format=None):
    count = 0
    if not format:
        format = extension(fastx_file)
    for seq_rec in SeqIO.parse(fastx_file, format):
        count += 1
    return count

def fetch_accessions_trim_format(reads_file,file_format=None):

    if file_format is None:
        file_format = reads_file.split('.')[-1]
    accessions = list()
    for seqRecord in SeqIO.parse(reads_file, file_format):
        accessions.append( trim_accession(seqRecord.name))
    #print accessions

    #print  "Number of accessions in " + reads_file + ":" + str(len(accessions))
    return accessions


def executing(cmdline, dump=True, test=False):
    if dump:
        print "EXECUTING %s" % (cmdline) + " . . . ."

    if not test:
        os.system(cmdline)


def get_named_item(list,names,name):
    """given a list and a set of names for the keys in the values, return the value which as name as the key """
    pos = names.index(name)
    #if pos < 0: raise Exception("There is no element in list with that value!")
    return list[pos]


def get_proper_read_id(trimmed_read_id):
    return trimmed_read_id[0:trimmed_read_id.rfind(':')]


def tsv_fetch_rows_and_colnames(tsv_filename, id_colname=None, preprocess_id_function=None, fetcher_function=None,
                                with_id_colname=False):
    tsvfile = open(tsv_filename, "rb")

    tsv_dict_reader = csv.DictReader(tsvfile,delimiter='\t')

    if id_colname is None:
        id_colname = tsv_dict_reader.fieldnames[0]

    tsv_rows = OrderedDict()

    tsv_normal_reader = csv.reader(open(tsv_filename,"r"),delimiter="\t")

    # skip line with the headers
    tsv_normal_reader.next()

    id_col_pos = tsv_dict_reader.fieldnames.index (id_colname)

    colnames = tsv_dict_reader.fieldnames
    del (tsv_dict_reader)

    for row in tsv_normal_reader:
        id_val = row[   id_col_pos ]

        if not with_id_colname:
            del (row[id_col_pos])
        if  preprocess_id_function is not None:
            id_val = preprocess_id_function(id_val)

        if fetcher_function is not None:
            tsv_rows[id_val] = fetcher_function( row )
        else:
            tsv_rows[id_val] = row

    if not with_id_colname:
        del (colnames[id_col_pos])

    return tsv_rows, colnames


def tsv_fetch_rows(tsv_filename,id_colnum = 0, preprocess_id_function=None,fetcher_function=None):
    """Fetch all rows from a tsv detecting firstly if the file has an header, if it has, skip it,
    does not use an dictionary to store the content of the rows"""
    tsvfile = open(tsv_filename, "r")

    sniffer = csv.Sniffer()

    sample = tsvfile.readline()

    tsvfile.seek(0)
    tsv_rows = OrderedDict()
    tsv_reader = csv.reader(open(tsv_filename, "r"), delimiter="\t")
    try:
        if (sniffer.has_header(sample)):
            tsv_reader.next()  # skips the header


        for row in tsv_reader:
            id_val = row[id_colnum]

            if preprocess_id_function != None:
                id_val = preprocess_id_function(id_val)

            if fetcher_function != None:
                tsv_rows[id_val] = fetcher_function( row )
            else:
                tsv_rows[id_val] = row
    except csv.Error:
        print "Some error happenned reading " + tsv_filename + ". Probabily this file is empty..."
    return tsv_rows

def pickle_to_file(something,name_of_the_file):
    pickle.dump(something,open(name_of_the_file,"wb"))
    print "Pickled ", len(something), " a ", type(something), " to ", name_of_the_file

def unpickle_from_file(pickled_things_filename):
    something = pickle.load(open(pickled_things_filename,'rb'))
    print "Unpickled ", len(something), " records from a ", type(something), ", from the file ", pickled_things_filename
    return something

def write_list_to_textfile(list,filename):
    open(filename,"w").write( "\n".join(list) + "\n" )

def list_lines_from_file(filename):
    return [ x.strip('\n') for x in open(filename,"r").readlines() ]

def num_cpus():
    return mp.cpu_count()


def walk_sequences(sequences_file, do_something_with, **callback_params):
    """
    walks through the file sequences_file , executing the callback do_do_something_with for every record
    the parameters for the callback are provided as the multiple final arguments

    Positional arguments:
        * do_something_with -- the function name to be called as callback
        * sequences_file -- the file with the sequences to be parsed

    Named arguments:
        * callback_params -- the parameters to be used on the callback
        * format -- format the sequences file format, if not provided, is guessed from the file extension

    Return values:
        if the callback returns any value, these ones are all returned in a list

    """

    if not callback_params.has_key('format'):
        format = extension(sequences_file)
    else:
        format = callback_params['format']

    records = SeqIO.parse(sequences_file, format)

    callback_param_count = do_something_with.func_code.co_argcount

    something = None

    for seq_rec in records:

        result = None

        if callback_param_count > 1:
            result = do_something_with(seq_rec, callback_params)
        else:
            result = do_something_with(seq_rec)

        if result:
            if type(result) is tuple:
                if something is None:
                    something = OrderedDict()
                something[result[0]] = result[1]
            else:
                if something is None:
                    something = list()
                something.append(result)

    return something


def sequence_quality_sum(seq_rec):
    """returns the sum of quality values of :seq_rec"""
    return sum(seq_rec.letter_annotations['phred_quality'])


def sequence_id_quality_sum(seq_rec):
    """return the qualities sum along with the id for some read"""
    return seq_rec.id, sequence_quality_sum(seq_rec)


def write_single_seq_record(seq_rec, args):
    """"""
    if not args.has_key('target_dir'):
        print "write_single_seq_record(): target dir to write to not provided! aborting!"
        return

    target_dir = args['target_dir']

    tokens = seq_rec.name.split('|')

    if len(tokens) > 1:
        seq_name = tokens[1]
        seq_rec.name = seq_name
        seq_rec.description = seq_name
        seq_rec.id = seq_name
    else:
        seq_name = seq_rec.name

    filename = target_dir + os.sep + seq_name + '.fasta'

    if filename.find(':') != -1:
        filename = filename.replace(':', '_')

    SeqIO.write(seq_rec, filename, 'fasta')


def fastq_scores(fastq_name):
    i = 0
    rec = 0
    counts = Counter()
    length_sum = 0
    for line in open(fastq_name):
        i += 1
        line = line.strip()
        if (i % 4 == 0):
            rec += 1
            scores_for_line = grab_scores(line)
            line_scores_counter = Counter(scores_for_line)
            #           print scores_for_line
            #           print "-"
            counts += line_scores_counter
            length_sum += len(line)
            if (rec % 10000) == 0: print(rec)
    # print "sum of all lengths:", length_sum
    return counts


def fastq_scores_mp(fastq_name, queue_limit=None):
    i = 0
    rec = 0
    pool = mp.Pool()
    counts = Counter()
    length_sum = 0
    p1, p2 = mp.Pipe()
    queue_lines = list()
    filler = 0

    lock = mp.Lock()
    if not queue_limit:
        queue_limit = 10000

    for line in open(fastq_name):
        i += 1
        line = line.strip()

        if (i % 4 == 0):
            queue_lines.append(line)
            if (filler > 10000):
                print rec
                filler = 0
            else:
                filler += 1

        if len(queue_lines) >= queue_limit:
            rec += len(queue_lines)
            counters_for_lines = pool.map(count_scores, queue_lines)
            counts = sum_count_with_list_counts(counts, counters_for_lines)
            queue_lines = []

    if len(queue_lines) > 0:
        rec += len(queue_lines)
        counters_for_lines = pool.map(count_scores, queue_lines)
        counts = sum_count_with_list_counts(counts, counters_for_lines)

    return counts


def sum_count_with_list_counts(count1, list_counts):
    for count in list_counts:
        count1 += count
    return count1


def count_scores(line):
    scores_for_line = grab_scores(line)
    return Counter(scores_for_line)

def grab_scores(line):
    scores = map(lambda c: ord(c) - 33, line)
    return scores


def write_to_report(lines):
 if os.environ['REPORT_FILE']:
    report_filename = os.environ['REPORT_FILE']
    open(report_filename, "a").writelines(line + "\n" for line in lines)

def dump(x):
    pprint.pprint(x,indent=3)


def string_last_changing_idx(id_list):
    last_id = str(id_list[0])
    for id in id_list:
        id_str = str(id)
        next_unchanging_pos = len(id_str)
        for i in xrange(0,next_unchanging_pos):
            if id_str[i] != last_id[i]:
                next_unchanging_pos = i
                break
        last_id = id_str
    return next_unchanging_pos

"""return the local time as a timestamp"""
def timestamp():
    return time.strftime(r"%Y%m%d_%H%M%S", time.localtime())

def callback_collect_fasta_header(seq_rec):
#    return seq_rec.name,seq_rec.description.split(';')[1]
    return seq_rec.name.split(';')[0:2]

def callback_fasta_write_one_rec(seq_rec,args):
    writer = args['writer']

    #seq_rec.description = ''
 #   SeqIO.write(seq_rec,fp,'fasta')
    tokens = seq_rec.name.split(';')
    seq_rec.id = tokens[0]

    seq_rec.description = ''
    writer.write_record(seq_rec)

def fasta_split_annotations(fasta_name):
    """given a FASTA file with annotated sequences in the headers, writes two files: one
    FASTA with just the accessions and another TSV with the accession with only the annotations"""

    #1st pass: grab the accessions and the annotations
    accs_annots_dict = walk_sequences(fasta_name,callback_collect_fasta_header,format='fasta')

    tsv_name = fasta_name + ".annots.tsv"
    writer = csv.DictWriter(open(tsv_name,"w"),delimiter='\t',fieldnames=['acc','annot'])
    for pair in accs_annots_dict:
        (acc,annot) = pair[0:2]
        writer.writerow({'acc':acc,'annot':annot})

    new_fasta = basename(fasta_name) + ".no_annot.fasta"

    fp = open(new_fasta,'w')


    writer = SeqIO.FastaIO.FastaWriter(fp,wrap=None)
    writer.write_header()
    #2nd pass: write a new fasta with just the accessions, discarding the annotations
    walk_sequences(fasta_name,callback_fasta_write_one_rec,format='fasta',writer=writer)

    writer.write_footer()

    fp.close()
