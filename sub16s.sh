#!/bin/bash

## do all the work of sub16s for one dataset, from the processing 'til the
## posprocessing Unifrac computations
##
## at the end will send an email to $EMAIL_TO virtual env specified below with
## the TSV with unifrac values as attachment
##
## example of usage:
##
## ./sub16s.sh dataset1
##


DATASET=$1

. analyzer.sh $DATASET $DATASET-biom
. posprocessor.sh $DATASET-biom
