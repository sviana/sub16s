#!/bin/bash
##initialize the environment variables needed to run SUB16S
##all of these env vars will be used by the downstream scripts
##it should be always run first any further execution steps

# verifies if the virtual env, if not tries to activate it
if [ ! -z $VIRTUAL_ENV ] ; then
	 echo "$VIRTUAL_ENV is already active!"
else
	. ./sub16s-venv/bin/activate
fi



# ensures thee script is not ran again
# when the environment is already setted!
if [ -v SUB16S_ENV_INIT ] ; then
		echo "SUB16S ENV already initialized!"
		echo "QUITTING!"
	return 0
fi




SUB16S_ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo -e "SUB16S is located at $SUB16S_ROOT_DIR"


#set verbose mode
set -x


SUB16S_SCRIPTS_DIR=$SUB16S_ROOT_DIR/scripts
PIPELINES_SCRIPTS_DIR=$SUB16S_ROOT_DIR/pipelines-scripts
BIOM_TOOLS_DIR=$SUB16S_ROOT_DIR/biom-tools
export PIPELINES_SCRIPTS_DIR
export SUB16S_ROOT_DIR
export SUB16S_SCRIPTS_DIR
export BIOM_TOOLS_DIR

#finds out the number of CPUs, useful in case the pipeline suports parallel processing
if [ ! -v NUM_CPUS ] ; then
	NUM_CPUS=`cat /proc/cpuinfo | grep processor | wc -l`
	export NUM_CPUS
fi

# loads the specific configuration for this host
. $SUB16S_ROOT_DIR/hostconfigs/`hostname`.config


export MOTHUR_DB_DIR
export QIIME_DB_DIR
export USEARCH_DB_DIR

#export TAXON_NUMBERER_DIR
export MOTHUR_WRAPPER_DIR
export UNIFRAC_SIM_DIR
#export MYPYCOMMONS_DIR

# set PYTHONPATH to include lib
PYTHONPATH=$PYTHONPATH:$SUB16S_ROOT_DIR/lib

QIIME_GLOBAL_SCRIPTS_DIR=/usr/lib/qiime/bin
set -x
# verifies if qiime is already installed and available in the path
qiime_installed=`which print_qiime_config.py`
qiime_globally=`find $QIIME_GLOBAL_SCRIPTS_DIR/print_qiime_config.py`

QIIME_SCRIPTS_DIR=$QIIME_GLOBAL_SCRIPTS_DIR
if [ ! ${#qiime_installed} -eq 0 ] ; then
	QIIME_SCRIPTS_DIR=`dirname $qiime_installed`
elif [ ! ${#qiime_globally} -eq 0 ]; then
	QIIME_SCRIPTS_DIR=`dirname $qiime_globally`
fi

echo "QIIME script are located on $QIIME_SCRIPTS_DIR"

# set new PATH
PATH=$PATH:$MOTHUR_WRAPPER_DIR:$UNIFRAC_SIM_DIR:$SUB16S_ROOT_DIR:$SUB16S_SCRIPTS_DIR:$BIOM_TOOLS_DIR:$QIIME_SCRIPTS_DIR

set +x
export PATH
export PYTHONPATH
export SUB16S_ENV_INIT=1

echo "SUB16S ENV correctly initialized!"
