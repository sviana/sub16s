#!/bin/bash
set -x
BIOM_OUTDIR=$1
DATASET=`echo $BIOM_OUTDIR | cut -d- -f1`

NAMES_OUTDIR=$DATASET-names
CONTROL_FILE=$DATASET-control.txt

QTYS_OUTDIR=$DATASET-qtys
CONTROL_TSV=$DATASET-control.tsv

if [ -z $BIOM_TOOLS_DIR ] ; then
	BIOM_TOOLS_DIR=$SUB16S_ROOT_DIR/biom-tools
	PATH=$PATH:$BIOM_TOOLS_DIR
fi

#process BIOM files, writing a new one with no singletons in it

for biom in $BIOM_OUTDIR/*.biom.json; do
	biom_remove_singles.py $biom
done

# using the BIOM files, generate two tabular files with weighted and unweigthed Unifrac similiarity
# for each BIOM file
. $SUB16S_SCRIPTS_DIR/unweighted_unifrac.sh $BIOM_OUTDIR $NAMES_OUTDIR $CONTROL_FILE
. $SUB16S_SCRIPTS_DIR/weighted_unifrac.sh $BIOM_OUTDIR $QTYS_OUTDIR $CONTROL_TSV

#send an email notifying about the conclusion

if [ $SUB16S_SEND_EMAIL -eq 1 ] ; then

	echo "Sending email"
	for type in weighted unweighted ; do
		echo -e "Here goes $type $DATASET" | send_email_attach.py \
			--sender_email $SUB16S_EMAIL_FROM \
			--sender_name "$SUB16S_SENDER_NAME" \
			--subject "SUB16S: $type unifrac measures about $DATASET is ready!"\
			--smtp_server $SUB16S_STMP_SERVER \
			--to $SUB16S_EMAIL_TO \
			--attach_file $DATASET.$type.unifrac.cleaned.tsv
	done

fi
